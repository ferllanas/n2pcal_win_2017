//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa. Com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PCalID_h__
#define __N2PCalID_h__

#include "SDKDef.h"

// Company:
#define kN2PCalCompanyKey	"INTERLASA"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2PCalCompanyValue	"INTERLASA"	// Company name displayed externally.

// Plug-in:
#define kN2PCalPluginName	"N2PCal"			// Name of this plug-in.
#define kN2PCalPrefixNumber	0xA3400 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2PCalVersion		"2.0"						// Version of this plug-in (for the About Box).
#define kN2PCalAuthor		"Interlasa S.A de C.V"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kN2PCalPrefixNumber above to modify the prefix.)
#define kN2PCalPrefix		RezLong(kN2PCalPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2PCalStringPrefix	SDK_DEF_STRINGIZE(kN2PCalPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2PCalMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2PCalMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2PCalPluginID, kN2PCalPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kN2PCalActionComponentBoss, kN2PCalPrefix + 0)
DECLARE_PMID(kClassIDSpace, kN2PCalDialogBoss, kN2PCalPrefix + 2)
DECLARE_PMID(kClassIDSpace, kICalRollOverIconButtonObserverBoss, kN2PCalPrefix + 3)
DECLARE_PMID(kClassIDSpace, kN2PCalTableCellWidgetBoss, kN2PCalPrefix + 4)
DECLARE_PMID(kClassIDSpace, kN2PCalTextEditBoxWidgetBoss, kN2PCalPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kN2PCalBoss, kN2PCalPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALTABLECELLDATA, kN2PCalPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IN2PCALINTERFACE, kN2PCalPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kN2PCalActionComponentImpl, kN2PCalPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kN2PCalDialogControllerImpl, kN2PCalPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kN2PCalDialogObserverImpl, kN2PCalPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kN2PCalTableCellDataImpl, kN2PCalPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kN2PCalEditBoxEventHandlerImpl, kN2PCalPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kN2PCalEditBoxObserverImpl, kN2PCalPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kN2PCalTableCellViewImpl, kN2PCalPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kN2PCalTableCellEventHImpl, kN2PCalPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kN2PCalTableCellObserverImpl, kN2PCalPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kICalRollOverButtonObserverImpl, kN2PCalPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kN2PCalImpl, kN2PCalPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kN2PCalAboutActionID, kN2PCalPrefix + 0)

DECLARE_PMID(kActionIDSpace, kN2PCalDialogActionID, kN2PCalPrefix + 4)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kN2PCalActionID, kN2PCalPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kN2PCalDialogWidgetID, kN2PCalPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kN2PCalComboBoxAnoWidgetID, kN2PCalPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kN2PCalComboBoxMesStatusWidgetID, kN2PCalPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kN2PCalTableCellWidgetID, kN2PCalPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kN2PCalTableCellEditBoxWidgetID, kN2PCalPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kN2PCalTableCellHScrollWidgetID, kN2PCalPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kN2PTableCellScrollWidgetID, kN2PCalPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kN2PCalWidgetID, kN2PCalPrefix + 25)


// "About Plug-ins" sub-menu:
#define kN2PCalAboutMenuKey			kN2PCalStringPrefix "kN2PCalAboutMenuKey"
#define kN2PCalAboutMenuPath		kSDKDefStandardAboutMenuPath kN2PCalCompanyKey

// "Plug-ins" sub-menu:
#define kN2PCalPluginsMenuKey 		kN2PCalStringPrefix "kN2PCalPluginsMenuKey"
#define kN2PCalPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2PCalCompanyKey kSDKDefDelimitMenuPath kN2PCalPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kN2PCalAboutBoxStringKey	kN2PCalStringPrefix "kN2PCalAboutBoxStringKey"
#define kN2PCalTargetMenuPath kN2PCalPluginsMenuPath

// Menu item positions:

#define kN2PCalDialogTitleKey kN2PCalStringPrefix "kN2PCalDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kN2PCalDialogMenuItemKey kN2PCalStringPrefix "kN2PCalDialogMenuItemKey"


#define kN2PCalAnoStringkey kN2PCalStringPrefix "kN2PCalAnoStringkey"
#define kN2PCalMesAnoStringkey kN2PCalStringPrefix "kN2PCalMesAnoStringkey"
#define kDiasSemStringkey kN2PCalStringPrefix "kDiasSemStringkey"

#define kN2PCalMesEneStringkey kN2PCalStringPrefix "kN2PCalMesEneStringkey"
#define kN2PCalMesFebStringkey kN2PCalStringPrefix "kN2PCalMesFebStringkey"
#define kN2PCalMesMarStringkey kN2PCalStringPrefix "kN2PCalMesMarStringkey"
#define kN2PCalMesAbrStringkey kN2PCalStringPrefix "kN2PCalMesAbrStringkey"
#define kN2PCalMesMayStringkey kN2PCalStringPrefix "kN2PCalMesMayStringkey"
#define kN2PCalMesJunStringkey kN2PCalStringPrefix "kN2PCalMesJunStringkey"
#define kN2PCalMesJulStringkey kN2PCalStringPrefix "kN2PCalMesJulStringkey"
#define kN2PCalMesAgoStringkey kN2PCalStringPrefix "kN2PCalMesAgoStringkey"
#define kN2PCalMesSepStringkey kN2PCalStringPrefix "kN2PCalMesSepStringkey"
#define kN2PCalMesOctStringkey kN2PCalStringPrefix "kN2PCalMesOctStringkey"
#define kN2PCalMesNovStringkey kN2PCalStringPrefix "kN2PCalMesNovStringkey"
#define kN2PCalMesDicStringkey kN2PCalStringPrefix "kN2PCalMesDicStringkey"


#define kN2PCalLunesStringkey   kN2PCalStringPrefix     "kN2PCalLunesStringkey"
#define kN2PCalMartesStringkey  kN2PCalStringPrefix     "kN2PCalMartesStringkey"
#define kN2PCalJuevesStringkey  kN2PCalStringPrefix     "kN2PCalJuevesStringkey"
#define kN2PCalMiercolesStringkey   kN2PCalStringPrefix "kN2PCalMiercolesStringkey"
#define kN2PCalViernesStringkey     kN2PCalStringPrefix "kN2PCalViernesStringkey"
#define kN2PCalSabadoStringkey  kN2PCalStringPrefix     "kN2PCalSabadoStringkey"
#define kN2PCalDomingoStringkey kN2PCalStringPrefix     "kN2PCalDomingoStringkey"


#define kN2PCalIdiomaStringkey kN2PCalStringPrefix "kN2PCalIdiomaStringkey"


// "Plug-ins" sub-menu item position for dialog:
#define kN2PCalDialogMenuItemPosition	12.0

#define kPanelWidth		196
#define kPanelHeight	245

#define kEditBoxHeight	65
#define kSizeBoxSize	16

#define kScrolBarWidth	16

#define	kMaxColumnData	7
#define	kMaxRowData		6
#define kDefaultCellWidth	25
#define kDefaultCellHeight	20
// Initial data format version numbers
#define kN2PCalFirstMajorFormatNumber  RezLong(1)
#define kN2PCalFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kN2PCalCurrentMajorFormatNumber kN2PCalFirstMajorFormatNumber
#define kN2PCalCurrentMinorFormatNumber kN2PCalFirstMinorFormatNumber

#endif // __N2PCalID_h__
