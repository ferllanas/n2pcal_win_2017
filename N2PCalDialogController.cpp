//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.Com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

// General includes:
#include "CDialogController.h"
#include "CAlert.h"

// Project includes:
#include "N2PCalID.h"
#include "IN2PCalTableCellData.h"

struct Date
{
    unsigned int day,
    month,
    year;
    Date(unsigned int D = 0,unsigned int M = 0,unsigned int Y = 0):
    day(D),month(M),year(Y){}; // CONSTRUCTOR
}origen1(1,1,1990);

const unsigned int BISIESTO = 1,
COMUN = 0,
x[2][13] = {{365,31,28,31,30,31,30,31,31,30,31,30,31},// comun
    {366,31,29,31,30,31,30,31,31,30,31,30,31}};// BISIESTO
const char *meses[13] = {"none","enero","febrero","marzo","abril",
    "mayo","junio","julio","agosto",
    "septiembre","octubre","noviembre","deciembre"},
*dias[7] = {"..domingo",
    "....lunes",
    "...martes",
    "miercoles",
    "...jueves",
    "..viernes",
    "...sabado"};

/** N2PCalDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup n2pcal
*/
class N2PCalDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PCalDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~N2PCalDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
    
        virtual void DialogCancelled();
    
private:
	
    void LlenadoComboAnos();
    void LlenadoComboBoxMes();
    void LlenadoTablaDiasDeMes();
    int32 GetAnoSelected();
    int32 GetMesSelected();
    void printmonth(Date d);
    unsigned int DiaEnLaSemana(Date &d);
    unsigned int GetYearType(unsigned int year);
};

CREATE_PMINTERFACE(N2PCalDialogController, kN2PCalDialogControllerImpl)

/* ApplyFields
*/
void N2PCalDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	CDialogController::InitializeDialogFields(dlgContext);
	// Put code to initialize widget values here.
    this->LlenadoComboAnos();
	this->LlenadoComboBoxMes();
	this->LlenadoTablaDiasDeMes();
}

/* ValidateFields
*/
WidgetID N2PCalDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void N2PCalDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// TODO add code that gathers widget values and applies them.
}

//METODO QUE HACE EL LLENADO DE A—OS EN EL COMBO
void N2PCalDialogController::LlenadoComboAnos()
{
	int32 posicion=-1;
	PMString NomPref="";
	do
	{
		struct tm *fh;
		time_t segundos;
		time(&segundos);
		fh=localtime(&segundos);
		int32 year= fh->tm_year+1900;
		
		PMString stringYearActual="";
		stringYearActual.AppendNumber(year);
		
		
		InterfacePtr<IWidgetParent>myParent(this, UseDefaultIID());
		if(myParent==nil)
		{
            CAlert::InformationAlert("panelData==nil");
			break;
		}
        
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
        {
            CAlert::InformationAlert("panelData==nil");
            break;
        }
        
		IControlView* ComboCView = panelData->FindWidget(kN2PCalComboBoxAnoWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
            CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			CAlert::InformationAlert("No pudo Obtener dropListData*");
			break;
		}
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
        if(IDDLDrComboBoxSelecPrefer==nil)
        {
            CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
            break;
        }
		
		int32 IndexSelected=0;
		if(dropListData->Length()>0)
		{
			IndexSelected=IDDLDrComboBoxSelecPrefer->GetSelected();
     		//PMString mns="IndexSelected=";
			//mns.AppendNumber(IndexSelected);
			//CAlert::InformationAlert(mns);
            
			if(IndexSelected>=0)
			{
				PMString StringSelected=dropListData->GetString(IndexSelected);
				dropListData->Clear(); //LIMPIA LA LISTA ACTUAL
				IndexSelected=-1;
				//Llena la lista
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			    for(int32 i = year-10;i<year+10;i++)
		   		{
		   			PMString yearString="";
		   			yearString.AppendNumber(i);
		   			dropListData->AddString(yearString, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
				}
                
				if(dropListData->Length()>0)
				{
					//busco la ultima preferencia con que se trabajo
					posicion = dropListData->GetIndex(StringSelected);
                    
					if(posicion<0)
					{
						IDDLDrComboBoxSelecPrefer->Select(0);
					}
					else
					{
						IDDLDrComboBoxSelecPrefer->Select(posicion);
					}
				}
				
			}
			else
			{
				//si no existe alguno seleccionado se limpia la lista
				//se vuelve a llenar busca el que contenga el termino default
				IndexSelected=-1;
				dropListData->Clear();//lIMPIA LA LISTA ACTUAL
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
		    	for(int32 i = year-10;i<year+10;i++)
		   		{
		   			PMString yearString="";
		   			yearString.AppendNumber(i);
		   			dropListData->AddString(yearString, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
				}
                
				if(dropListData->Length()>0)
				{
					//busco la ultima preferencia con que se trabajo
					posicion = dropListData->GetIndex(stringYearActual);
                    
					if(posicion<0)
					{
						IDDLDrComboBoxSelecPrefer->Select(0);
					}
					else
					{
						IDDLDrComboBoxSelecPrefer->Select(posicion);
					}
				}
			}
		}
		else
		{
            
			//no existen Path en combo llenado del combo
			IndexSelected=-1;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
            for(int32 i = year-10;i<year+10;i++)
            {
                PMString yearString="";
                yearString.AppendNumber(i);
                //ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
                dropListData->AddString(yearString, IStringListControlData::kEnd, kFalse, kFalse);
            }
			
            if(dropListData->Length()>0)
            {
                //busco la ultima preferencia con que se trabajo
                posicion = dropListData->GetIndex(stringYearActual);
                
                if(posicion<0)
                {
                    IDDLDrComboBoxSelecPrefer->Select(0);
                }
                else
                {
                    IDDLDrComboBoxSelecPrefer->Select(posicion);
                }
            }
		}
	}while(false);
}


//METODO QUE LLENA EL COMBO DE ACUERDO A TODOS LOS MESES
void N2PCalDialogController::LlenadoComboBoxMes()
{
	int32 posicion=-1;
	PMString NomPref="";
	do
	{
		struct tm *fh;
		time_t segundos;
		time(&segundos);
		fh=localtime(&segundos);
		int32 month = fh->tm_mon;
        
		PMString stringYearActual="";
		stringYearActual.AppendNumber(month);
		
		InterfacePtr<IWidgetParent>myParent(this, UseDefaultIID());
		if(myParent==nil)
		{
            CAlert::InformationAlert("LlenadoComboBoxMes myParent==nil");
			break;
		}
        
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
        {
             CAlert::InformationAlert("LlenadoComboBoxMes panelData==nil");
            break;
        }
        
		IControlView* ComboCView = panelData->FindWidget(kN2PCalComboBoxMesStatusWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
            CAlert::InformationAlert("LlenadoComboBoxMes ComboCView==nil");
            break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			CAlert::InformationAlert("LlenadoComboBoxMes dropListData==nil");
			break;
		}
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
        if(IDDLDrComboBoxSelecPrefer==nil)
        {
            CAlert::InformationAlert("LlenadoComboBoxMes IDDLDrComboBoxSelecPrefer==nil");
            break;
        }
		
		int32 IndexSelected=0;
		if(dropListData->Length()>0)
		{
			IndexSelected=IDDLDrComboBoxSelecPrefer->GetSelected();
			
			if(IndexSelected>=0)
			{
				IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
			}
			else
			{
		    	IDDLDrComboBoxSelecPrefer->Select(month);
			}
		}
	}while(false);
}



void N2PCalDialogController::LlenadoTablaDiasDeMes()
{
	do
	{
		/*	int32 mm   = 0; ////////////////////////////////////////////////////////////////
         int32 dd   = 1; //
         int32 leap = 0; //	MAIN FUCNTION VARIABLE DECLARATION.
         int32 yyyy = 0; //
         int32 day  = 0;
         
         yyyy=this->GetAnoSelected();
         
         mm=this->GetMesSelected();
         */
        
		Date d;
		d.year = this->GetAnoSelected();;
		d.month = this->GetMesSelected();
		d.day = 1;
		
		//this->getday(d);
		this->printmonth(d);
	}while(false);
	
}
//OBTENIENE A—O SELECIONADO
int32 N2PCalDialogController::GetAnoSelected()
{
	PMString stringAno="";
	do
	{
		InterfacePtr<IWidgetParent>myParent(this, UseDefaultIID());
		if(myParent==nil)
		{
            CAlert::InformationAlert("GetAnoSelected myParent");
			break;
		}
        
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
        {
            CAlert::InformationAlert("GetAnoSelected panelData");
            break;
        }
        
		IControlView* ComboCView = panelData->FindWidget(kN2PCalComboBoxAnoWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			CAlert::InformationAlert("GetAnoSelected ComboCView");
			break;
		}
		
        
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			CAlert::InformationAlert("GetAnoSelected dropListData");
			break;
		}
		
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
        if(IDDLDrComboBoxSelecPrefer==nil)
        {
            CAlert::InformationAlert("GetAnoSelected IDDLDrComboBoxSelecPrefer");
            break;
        }
        
		int32 index=IDDLDrComboBoxSelecPrefer->GetSelected();
		
		stringAno=dropListData->GetString(index);
		
	}while(false);
    
	return(stringAno.GetAsNumber());
}


int32 N2PCalDialogController::GetMesSelected()
{
	int32 Mesindex=-1;
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent==nil)
		{
            CAlert::InformationAlert("GetAnoSelected myParent");
			break;
		}
        
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
        {
            CAlert::InformationAlert("GetAnoSelected panelData");
            break;
        }
        
		IControlView* ComboCView = panelData->FindWidget(kN2PCalComboBoxMesStatusWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			CAlert::InformationAlert("GetAnoSelected ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			CAlert::InformationAlert("GetAnoSelected dropListData");
			break;
		}
		
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
        if(IDDLDrComboBoxSelecPrefer==nil)
        {
            CAlert::InformationAlert("GetAnoSelected IDDLDrComboBoxSelecPrefer");
            break;
        }
        
		Mesindex=IDDLDrComboBoxSelecPrefer->GetSelected();
        
	}while(false);
    return(Mesindex+1);
}

void N2PCalDialogController::printmonth(Date d)  // pointer to day value
{
    
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent == nil)
		{
            CAlert::InformationAlert("GetAnoSelected myParent");
			break;
		}
		InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
        {
            CAlert::InformationAlert("GetAnoSelected panelData");
            break;
        }
		InterfacePtr<IN2PCalTableCellData>	pTableCellData1(this, UseDefaultIID());
		if(pTableCellData1 == nil)
		{
            CAlert::InformationAlert("GetAnoSelected pTableCellData1");
			ASSERT_FAIL("Error1");
		}
        
		for(int32 Col=0;Col<pTableCellData1->GetMaxColumn();Col++)
        {
            PMString SX="";
            SX.AppendNumber(Col);
			for(int32 Row=0;Row<pTableCellData1->GetMaxRow();Row++)
			{
                SX.AppendNumber(Col);
                SX.Append(",");
                SX.AppendNumber(Row);
				pTableCellData1->SetCellString(WideString(""), Col,Row);
				InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
				if(tableCell == nil)
                {
                    CAlert::InformationAlert("GetAnoSelected tableCell");
                    break;
                }
				tableCell->Invalidate();
			}
        }
		
		d.day = 1;
		int index = DiaEnLaSemana(d);
		int counter = 0;
		//int z=numday;
		for( counter = 0;counter < index;counter++)
		{
			pTableCellData1->SetCellString(WideString("-"), counter,0);
			InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
			if(tableCell == nil)
            {
                CAlert::InformationAlert("GetAnoSelected tableCell");
				break;
            }
			
			tableCell->Invalidate();
			//cout << setw(5) << "-";
		}
		int32 row=0;
		for(int dia = 1;dia <= x[GetYearType(d.year)][d.month];counter++,dia++)
		{
            
			if(counter%7 == 0 && dia == 1)
			{
                PMString NumDayString="";
                NumDayString.AppendNumber(dia);
                
                pTableCellData1->SetCellString(WideString(NumDayString), index,row);
                
                InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
                if(tableCell == nil)
                {
                    CAlert::InformationAlert("GetAnoSelected tableCell");
                    break;
                }
                
                tableCell->Invalidate();
			}
			else
			{
				PMString NumDayString="";
				NumDayString.AppendNumber(dia);
                
				pTableCellData1->SetCellString(WideString(NumDayString), index,row);
                
				InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
				if(tableCell == nil)
                {
                    CAlert::InformationAlert("GetAnoSelected tableCell");
                    break;  
                }
                
				tableCell->Invalidate();
                
				if(counter%7 == 0)
				{
					index=0;
					row++;
					
                    
					NumDayString="";
					NumDayString.AppendNumber(dia);
                    
					pTableCellData1->SetCellString(WideString(NumDayString), index,row);
                    
					InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
					if(tableCell == nil)
                    {
                        CAlert::InformationAlert("GetAnoSelected tableCell");
						break;
                    }
					tableCell->Invalidate();
				}
			}
			index++;
		}
	}while(false);
}

void N2PCalDialogController::DialogCancelled()
{
	this->SetTextControlData(kN2PCalTableCellEditBoxWidgetID," ");
}

unsigned int N2PCalDialogController::DiaEnLaSemana(Date &d)
{
	unsigned int qty = 0;
	for(unsigned int y = origen1.year;y < d.year;y++)
		qty += x[GetYearType(y)][0];
	int index = GetYearType(d.year);
	for(unsigned int m = 1;m < d.month;m++)
		qty += x[index][m];
	qty += d.day;
	return qty%7;
} // DIA EN LA SEMANA


unsigned int N2PCalDialogController::GetYearType(unsigned int year)
{
	if(year%100 == 0) // si year es divisible por 4 pero termina en 00
        year /= 100; // es BISIESTO solamente cuando es divisible por 400
	if(year%4) // year no es divisible por 4,
        return COMUN; // por eso no es BISIESTO.
	return BISIESTO;
} // GET YEAR TYPE

