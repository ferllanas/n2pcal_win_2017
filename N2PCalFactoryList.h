//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.Com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================
REGISTER_PMINTERFACE(N2PCalActionComponent,     kN2PCalActionComponentImpl)
REGISTER_PMINTERFACE(N2PTableCellData,          kN2PCalTableCellDataImpl)
REGISTER_PMINTERFACE(N2PCalDialogController,    kN2PCalDialogControllerImpl)
REGISTER_PMINTERFACE(N2PCalDialogObserver,      kN2PCalDialogObserverImpl)
REGISTER_PMINTERFACE(N2PTableCellEventHandler,  kN2PCalTableCellEventHImpl)
REGISTER_PMINTERFACE(N2PTableCellObserver,      kN2PCalTableCellObserverImpl)
REGISTER_PMINTERFACE(N2PTableCellView,          kN2PCalTableCellViewImpl)

