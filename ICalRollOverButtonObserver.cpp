//========================================================================================
//DATE: 10/09/08
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Interface includes
#include "ISubject.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// Implem includes

#include "CAlert.h"
#include "CObserver.h"
#include "N2PCalID.h"

/**
	Adding an IObserver interface to a widget boss class creates a button
	whose presses can be registered as Update messages to the client code. 

	@ingroup pictureicon
	
*/
class ICalRollOverButtonObserver : public CObserver
{
public:
	/**
		Constructor 
	*/		
	ICalRollOverButtonObserver(IPMUnknown *boss);
	/**
		Destructor
	*/
	~ICalRollOverButtonObserver();
	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called when the 
		widget boss object is shown.
	*/		
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when the widget is hidden.
	*/		
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

	
};

CREATE_PMINTERFACE(ICalRollOverButtonObserver, kICalRollOverButtonObserverImpl)


ICalRollOverButtonObserver::ICalRollOverButtonObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


ICalRollOverButtonObserver::~ICalRollOverButtonObserver()
{
	
}


void ICalRollOverButtonObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA);
	}
}


void ICalRollOverButtonObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ITRISTATECONTROLDATA);
	}
}



void ICalRollOverButtonObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if(theChange == kTrueStateMessage) 
	{
		// No behaviour in this sample: but indicate at least that the message has been received.
		// Dialog-specific resource includes:
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PCalPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil)
		{
			break;
		}


		// Open the dialog.
		dialog->Open(); 
		CAlert::InformationAlert("boton ok FECHA");
	
	 } while (false);	
   }	
}