//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/cellpanel/N2PTableCellEventHandler.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #2 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IEventDispatcher.h"
#include "ITextControlData.h"
#include "IPanelControlData.h"			/* <IPanelControlData> */
#include "IWidgetParent.h"				/* <IWidgetParent>     */
#include "IWindowPort.h"				/* <IWindowPort>   */
#include "IN2PCalTableCellData.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

// General includes:
#include "CEventHandler.h"
#include "DVControlEventHandler.h"
#include "AcquireViewPort.h"
#include "CAlert.h"

// Project includes:
#include "N2PCalID.h"


/** 
	When the user clicks on the table, this implementation calculates the coordinate of the clicked cell in the table.
	
	@ingroup cellpanel
	
*/
class N2PTableCellEventHandler : public DVControlEventHandler
{
    typedef DVControlEventHandler inherited;
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PTableCellEventHandler(IPMUnknown *boss): inherited(boss),
													fColCount(0), fRowCount(0),
													fCellWidth(0), fCellHeight(0)
		{};
		/**
			Destructor.
		*/
		~N2PTableCellEventHandler();
		/**
			Left mouse button released.
			param e Contains data about the mouse event.
		*/
		virtual bool16	LButtonUp(IEvent* e);
	
	private:
		/** Private method */
		bool16			FindWhichPane(IEvent* e, bool16 drawSelection);
		void			GetTableCellValues(void);
		void			InvalHiliteBox( IControlView *myView);
		
	private:
		/** Private data member */		
		int32			fColCount, fRowCount;
		int32			fCellWidth,	fCellHeight;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(N2PTableCellEventHandler, kN2PCalTableCellEventHImpl)

/* N2PTableCellEventHandler
*/
N2PTableCellEventHandler::~N2PTableCellEventHandler()
{
	
}

/* LButtonUp
*/
bool16 N2PTableCellEventHandler::LButtonUp(IEvent* e)
{
	GetTableCellValues();
	
	if (FindWhichPane(e, kTrue)){
 		return kTrue;
	}else
		return kFalse;
}

/* FindWhichPane   Encuentra la fecha en el dialogo del calendario*/
bool16 N2PTableCellEventHandler::FindWhichPane(IEvent *e, bool16 drawSelection)
{
	do
    {
		InterfacePtr<IControlView>		myView(this, UseDefaultIID());
		if(myView == nil) break;
		int32	maxViewRow		= fRowCount - 1;
		int32	maxViewColumn	= fColCount - 1;
		SysPoint	pt = e->GlobalWhere();
		SysPoint	localWhere = ::ToSys(myView->GlobalToWindow(pt));
		SysRect		viewBBox = myView->GetBBox();
		int32	column = (int32)(SysPointH(localWhere) - SysRectLeft(viewBBox));
        
        
		int32	row = (int32)(SysPointV(localWhere) - SysRectTop(viewBBox));
        
        if(fCellWidth<=0)
        {
            PMString HR="";
            HR.AppendNumber(column);
            HR.Append(",");
            HR.AppendNumber(fCellWidth);
            CAlert::InformationAlert("Error Algo paso: "+HR);
            break;
        }
        
		column /= fCellWidth;
		row /= fCellHeight;
		if (!((0 <= row) && (0 <= column) && (row < fRowCount) && (column < fColCount)))
		{
			row = column = -1;
			drawSelection = kFalse;
		}
		if(!drawSelection) break;
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent == nil) break;
		InterfacePtr<IN2PCalTableCellData>	pTableCellData((IN2PCalTableCellData*)myParent->QueryParentFor(IID_IN2PCALTABLECELLDATA));
		if(pTableCellData == nil) break;
		InvalHiliteBox(myView);
		pTableCellData->SetSelectRow(row);
		pTableCellData->SetSelectColumn(column);
		InvalHiliteBox(myView);
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(pTableCellData == nil) break;
		InterfacePtr<IControlView>		editBoxView(panel->FindWidget(kN2PCalTableCellEditBoxWidgetID), UseDefaultIID());
		if(editBoxView == nil) break;
		InterfacePtr<ITextControlData>	selectedChar(editBoxView, UseDefaultIID());
		if(selectedChar == nil) break;
		row = pTableCellData->GetSelectRow();
		column = pTableCellData->GetSelectColumn();
		//PMString s = pTableCellData->GetCellString(column, row);
        //s.SetTranslatable(kFalse);
		//selectedChar->SetString(s, kTrue, kFalse);
        
        PMString assetPathTitle(kN2PCalIdiomaStringkey);
		assetPathTitle.Translate();
		assetPathTitle.SetTranslatable(kTrue);
		
        
		PMString NumDiadeMes = pTableCellData->GetCellString(column, row);
		NumDiadeMes.SetTranslatable(kFalse);
        
		
        IControlView* ComboCViewComboBoxAno = panel->FindWidget(kN2PCalComboBoxAnoWidgetID);
		ASSERT(ComboCViewComboBoxAno);
		if(ComboCViewComboBoxAno == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		
        
		InterfacePtr<IStringListControlData> dropListDataComboBoxAno(ComboCViewComboBoxAno,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataComboBoxAno == nil)
		{
			//ASSERT_FAIL("No pudo Obtener dropListDataComboBoxAno*");
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel dropListDataComboBoxAno");
			break;
		}
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxAnoPrefer( ComboCViewComboBoxAno, IID_IDROPDOWNLISTCONTROLLER);
        if(IDDLDrComboBoxAnoPrefer==nil)
        {
            //ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
            CAlert::InformationAlert("LlenarComboPreferenciasOnPanel IDDLDrComboBoxSelecPrefer");
            break;
        }
        
		
		
		
        PMString stringAno="";
        if(IDDLDrComboBoxAnoPrefer->GetSelected()>=0)
        {
            stringAno=dropListDataComboBoxAno->GetString(IDDLDrComboBoxAnoPrefer->GetSelected());
        }
        else
        {
            break;
        }
		//Get Mes
		
        
		IControlView* ComboCView = panel->FindWidget(kN2PCalComboBoxMesStatusWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		
        
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			//ASSERT_FAIL("No pudo Obtener dropListData*");
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel dropListData");
			break;
		}
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
        if(IDDLDrComboBoxSelecPrefer==nil)
        {
            //ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
            CAlert::InformationAlert("LlenarComboPreferenciasOnPanel IDDLDrComboBoxSelecPrefer");
            break;
        }
        
		PMString MesString ="";
		PMString nmnk="";
		nmnk.AppendNumber(IDDLDrComboBoxSelecPrefer->GetSelected());
		//CAlert::InformationAlert(nmnk);
		if(IDDLDrComboBoxSelecPrefer->GetSelected()>=0)
		{
			MesString = dropListData->GetString(IDDLDrComboBoxSelecPrefer->GetSelected());
			MesString.Translate();
			MesString.SetTranslatable(kTrue);
			//CAlert::InformationAlert(MesString);
		}
		else
		{
			break;
		}
		//CADENA DEL DIA
		
		PMString Dia="";
		
		switch(column)
        {
            case 1:
                Dia.Append(kN2PCalLunesStringkey);
                Dia.Translate();
                Dia.SetTranslatable(kTrue);
				break;
				
            case 2:
                Dia.Append(kN2PCalMartesStringkey);
                Dia.Translate();
                Dia.SetTranslatable(kTrue);
				break;
				
            case 3:
                Dia.Append(kN2PCalMiercolesStringkey);
                Dia.Translate();
                Dia.SetTranslatable(kTrue);
				break;
				
            case 4:
                Dia.Append(kN2PCalJuevesStringkey);
                Dia.Translate();
                Dia.SetTranslatable(kTrue);
				break;
				
            case 5:
                Dia.Append(kN2PCalViernesStringkey);
                Dia.Translate();
                Dia.SetTranslatable(kTrue);
				break;
				
            case 6:
                Dia.Append(kN2PCalSabadoStringkey);
                Dia.Translate();
                Dia.SetTranslatable(kTrue);
				break;
				
            case 0:
                Dia.Append(kN2PCalDomingoStringkey);
                Dia.Translate();
                Dia.SetTranslatable(kTrue);
                
				break;
        }
        
		
		PMString Date="";
		//CAlert::InformationAlert("X3="+NumDiadeMes);
		if(NumDiadeMes.NumUTF16TextChars()>0 )//&& NumDiadeMes.GetAsNumber()>0)
		{
			//CAlert::InformationAlert("X2="+NumDiadeMes);
			//Forma Fecha por Idioma  assetPathTitle tiene la cadena para detectar el idioma
			if(assetPathTitle=="English")
			{
				Date.Append(Dia);
				Date.Append(", ");
				Date.Append(MesString);
				Date.Append(" ");
				Date.Append(NumDiadeMes);
				Date.Append(", ");
				Date.Append(stringAno);
			}
			else
			{
				if(assetPathTitle=="Espanol")
				{
					Date.Append(Dia);
					Date.Append(" ");
					Date.Append(NumDiadeMes);
					Date.Append(" de ");
					Date.Append(MesString);
					Date.Append("  de ");
					Date.Append(stringAno);	
				}
			}
			selectedChar->SetString(Date, kTrue, kFalse);
		}
		else
		{
			//CAlert::InformationAlert("X4="+NumDiadeMes);
			selectedChar->SetString(Date, kTrue, kFalse);
		}
	}
    /*{
        
        CAlert::InformationAlert("A1");
		InterfacePtr<IControlView>		myView(this, UseDefaultIID());
		if(myView == nil) break;
		
		int32	maxViewRow		= fRowCount - 1;
		int32	maxViewColumn	= fColCount - 1;
		
		//ViewPortAccess<IWindowPort> windowPort(this, IID_IWINDOWPORT);
		//AcquireViewPort aqViewPort(windowPort);

		SysPoint	pt = e->GlobalWhere();
		SysPoint	localWhere = ::ToSys(myView->GlobalToWindow(pt));

		SysRect		viewBBox = myView->GetBBox();
		
		int32	column = (int32)(SysPointH(localWhere) - SysRectLeft(viewBBox));
		int32	row = (int32)(SysPointV(localWhere) - SysRectTop(viewBBox));
		
         CAlert::InformationAlert("A2");
		column /= fCellWidth;
		row /= fCellHeight;

		if (!((0 <= row) && (0 <= column) && (row < fRowCount) && (column < fColCount)))
		{
			row = column = -1;
			drawSelection = kFalse;
		}
		
		 CAlert::InformationAlert("A3");
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent == nil) break;
		
		InterfacePtr<IN2PCalTableCellData>	pTableCellData(this, UseDefaultIID());
		if(pTableCellData == nil) break;
		
		InvalHiliteBox(myView);
		
		pTableCellData->SetSelectRow(row);
		pTableCellData->SetSelectColumn(column);
		 CAlert::InformationAlert("A4");
		InvalHiliteBox(myView);

					
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(pTableCellData == nil) break;
		
		InterfacePtr<IControlView>		editBoxView(panel->FindWidget(kN2PCalTableCellEditBoxWidgetID), UseDefaultIID());
		if(editBoxView == nil) break;
		
		InterfacePtr<ITextControlData>	selectedChar(editBoxView, UseDefaultIID());
		if(selectedChar == nil) break;

		 CAlert::InformationAlert("A5");
		row = pTableCellData->GetSelectRow();
		column = pTableCellData->GetSelectColumn();
		
		
		PMString assetPathTitle(kN2PCalIdiomaStringkey);
		assetPathTitle.Translate();
		assetPathTitle.SetTranslatable(kTrue);
		
			
		PMString NumDiadeMes = pTableCellData->GetCellString(column, row);
		NumDiadeMes.SetTranslatable(kFalse);

		
		 CAlert::InformationAlert("A6");
		///Get A�o
		IControlView* ComboCViewComboBoxAno = panel->FindWidget(kN2PCalComboBoxAnoWidgetID);
		ASSERT(ComboCViewComboBoxAno);
		if(ComboCViewComboBoxAno == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		

		InterfacePtr<IStringListControlData> dropListDataComboBoxAno(ComboCViewComboBoxAno,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataComboBoxAno == nil)
		{
			//ASSERT_FAIL("No pudo Obtener dropListDataComboBoxAno*");
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel dropListDataComboBoxAno");
			break;
		}
		 CAlert::InformationAlert("A7");
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxAnoPrefer( ComboCViewComboBoxAno, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxAnoPrefer==nil)
			{
				//ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
				CAlert::InformationAlert("LlenarComboPreferenciasOnPanel IDDLDrComboBoxSelecPrefer");
				break;
			}
			
		
		
		
			PMString stringAno="";
			if(IDDLDrComboBoxAnoPrefer->GetSelected()>=0)
			{
				stringAno=dropListDataComboBoxAno->GetString(IDDLDrComboBoxAnoPrefer->GetSelected());
			}
			else
			{
				break;
			}
		 CAlert::InformationAlert("A8");
		//Get Mes
		
			
		IControlView* ComboCView = panel->FindWidget(kN2PCalComboBoxMesStatusWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		

		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			//ASSERT_FAIL("No pudo Obtener dropListData*");
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel dropListData");
			break;
		}
		 CAlert::InformationAlert("A9");
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				//ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
				CAlert::InformationAlert("LlenarComboPreferenciasOnPanel IDDLDrComboBoxSelecPrefer");
				break;
			}
			
		PMString MesString ="";
		PMString nmnk="";
		nmnk.AppendNumber(IDDLDrComboBoxSelecPrefer->GetSelected());
		//CAlert::InformationAlert(nmnk);
		if(IDDLDrComboBoxSelecPrefer->GetSelected()>=0)
		{
			MesString = dropListData->GetString(IDDLDrComboBoxSelecPrefer->GetSelected());
			MesString.Translate();
			MesString.SetTranslatable(kTrue);
			//CAlert::InformationAlert(MesString);
		}
		else
		{
			break;
		}
		 CAlert::InformationAlert("A10");
		//CADENA DEL DIA
		
		PMString Dia="";
		
		switch(column)
			{
				case 1:
					Dia.Append(kN2PCalLunesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
				break;
				
				case 2:
					Dia.Append(kN2PCalMartesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
				break;
				
				case 3:
					Dia.Append(kN2PCalMiercolesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
				break;
				
				case 4:
					Dia.Append(kN2PCalJuevesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
				break;
				
				case 5:
					Dia.Append(kN2PCalViernesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
				break;
				
				case 6:
					Dia.Append(kN2PCalSabadoStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
				break;
				
				case 0:
					Dia.Append(kN2PCalDomingoStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
					
				break;
			}
			
		
			 CAlert::InformationAlert("A11");
		PMString Date="";
		//CAlert::InformationAlert("X3="+NumDiadeMes);
		if(NumDiadeMes.NumUTF16TextChars()>0 )//&& NumDiadeMes.GetAsNumber()>0)
		{
			//CAlert::InformationAlert("X2="+NumDiadeMes);
			//Forma Fecha por Idioma  assetPathTitle tiene la cadena para detectar el idioma
			if(assetPathTitle=="English")
			{
				Date.Append(Dia);
				Date.Append(", ");
				Date.Append(MesString);
				Date.Append(" ");
				Date.Append(NumDiadeMes);
				Date.Append(", ");
				Date.Append(stringAno);
			}
			else
			{
				if(assetPathTitle=="Espanol")
				{
					Date.Append(Dia);
					Date.Append(" ");
					Date.Append(NumDiadeMes);
					Date.Append(" de ");
					Date.Append(MesString);
					Date.Append("  de ");
					Date.Append(stringAno);	
				}
			}
			selectedChar->SetString(Date, kTrue, kFalse);
		}
		else
		{
			//CAlert::InformationAlert("X4="+NumDiadeMes);
			selectedChar->SetString(Date, kTrue, kFalse);
		}
		 CAlert::InformationAlert("A12");
		//CAlert::InformationAlert("X1="+Date);
		
	}*/while(false);
	
	return drawSelection;
}

/* GetTableCellValues   OBTIENE LA CELDA DE LOS DIAS EN LA CUADRICULA*/
void N2PTableCellEventHandler::GetTableCellValues(void)
{
	do{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent == nil)
        {
            CAlert::InformationAlert(" GetTableCellValues myParent == nil");
            break;
        }
        InterfacePtr<IN2PCalTableCellData>	pTableCellData((IN2PCalTableCellData*)myParent->QueryParentFor(IID_IN2PCALTABLECELLDATA));
		//InterfacePtr<IN2PCalTableCellData>	pTableCellData(this, UseDefaultIID());
		if(pTableCellData == nil)
        {
             CAlert::InformationAlert(" GetTableCellValues pTableCellData == nil");
            break;
        }
		
		fColCount	=	pTableCellData->GetCellNumberOfColumns();
		fRowCount	=	pTableCellData->GetCellNumberOfRows();
		fCellWidth	=	pTableCellData->GetCellWidth();
		fCellHeight	=	pTableCellData->GetCellHeight();

	}while(false);

	//CAlert::InformationAlert("busqueda de las fechas");
}

/* InvalHiliteBox*/
void N2PTableCellEventHandler::InvalHiliteBox( IControlView* myView)
{
	//if(windowPort == nil) return;
	if(myView == nil) return;
	
	do{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent == nil) break;
        InterfacePtr<IN2PCalTableCellData>	pTableCellData((IN2PCalTableCellData*)myParent->QueryParentFor(IID_IN2PCALTABLECELLDATA));
		//InterfacePtr<IN2PCalTableCellData>	pTableCellData(this, UseDefaultIID());
        if(pTableCellData == nil)break;
		
		int32	top = fCellHeight * (pTableCellData->GetSelectRow() - pTableCellData->GetViewTopRow());
		int32	left = fCellWidth * (pTableCellData->GetSelectColumn() - pTableCellData->GetViewLeftColumn());
		
		PMRect frame(left, top, left + fCellWidth, top + fCellHeight);
							
		myView->Invalidate(&frame);
	}while(false);
}

// End, TableCellEventH.cpp.
