//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.Com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "IWidgetParent.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ITextControlData.h"
// General includes:
#include "CDialogObserver.h"
#include "CAlert.h"

// Project includes:
#include "N2PCalID.h"
#include "IN2PCalTableCellData.h"

/** Implements IObserver based on the partial implementation CDialogObserver.@ingroup n2pcalendario*/

struct DateX
{
    unsigned int day,
    month,
    year;
    DateX(unsigned int D = 0,unsigned int M = 0,unsigned int Y = 0):
    day(D),month(M),year(Y){}; // CONSTRUCTOR
}origen(1,1,1990);

const unsigned int BISIESTOX = 1,
COMUNX = 0,
XX[2][13] = {{365,31,28,31,30,31,30,31,31,30,31,30,31},// comun
    {366,31,29,31,30,31,30,31,31,30,31,30,31}};// BISIESTOX
const char *mesesx[13] = {"none","enero","febrero","marzo","abril",
    "mayo","junio","julio","agosto",
    "septiembre","octubre","noviembre","deciembre"},
*diasx[7] = {"..domingo",
    "....lunes",
    "...martes",
    "miercoles",
    "...jueves",
    "..viernes",
    "...sabado"};

class N2PCalDialogObserver : public CDialogObserver
{
public:
    /*	Constructor.@param boss interface ptr from boss object on which this interface is aggregated.*/
    N2PCalDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}
    
    /** Destructor. */
    virtual ~N2PCalDialogObserver() {}
    
    /**
     Called by the application to allow the observer to attach to the subjects
     to be observed, in this case the dialog's info button widget. If you want
     to observe other widgets on the dialog you could add them here.
     */
    virtual void AutoAttach();
    
    /** Called by the application to allow the observer to detach from the subjects being observed. */
    virtual void AutoDetach();
    
    /*	Called by the host when the observed object changes, in this case when
     the dialog's info button is clicked.
     @param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
     @param theSubject points to the ISubject interface for the subject that has changed.
     @param protocol specifies the ID of the changed interface on the subject boss.
     @param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
     */
    virtual void Update
    (
     const ClassID& theChange,
     ISubject* theSubject,
     const PMIID& protocol,
     void* changedBy
     );
    
private:
	
    int32 GetAnoSelected();
    int32 GetMesSelected();
    void printmonth(DateX d);
    void CambiaFechaString();
    unsigned int DiaEnLaSemana(DateX &d);
    unsigned int GetYearType(unsigned int year);
};


/* CREATE_PMINTERFACE  Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.*/

CREATE_PMINTERFACE(N2PCalDialogObserver, kN2PCalDialogObserverImpl)

/* AutoAttach*/
void N2PCalDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Attach to other widgets you want to handle dynamically here.
        //AttachToWidget(kN2PCalPruebanWidgetID,           IID_IBOOLEANCONTROLDATA,    panelControlData);
		AttachToWidget(kN2PCalComboBoxMesStatusWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		AttachToWidget(kN2PCalComboBoxAnoWidgetID,       IID_ISTRINGLISTCONTROLDATA, panelControlData);
    } while (kFalse);
}

/* AutoDetach*/
void N2PCalDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Detach from other widgets you handle dynamically here.
       // DetachFromWidget(kN2PCalPruebanWidgetID,            IID_IBOOLEANCONTROLDATA,    panelControlData);
        DetachFromWidget(kN2PCalComboBoxMesStatusWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
        DetachFromWidget(kN2PCalComboBoxAnoWidgetID,       IID_ISTRINGLISTCONTROLDATA, panelControlData);
	} while (kFalse);
}

/* Update*/
void N2PCalDialogObserver::Update
(
 const ClassID& theChange,
 ISubject* theSubject,
 const PMIID& protocol,
 void* changedBy
 )
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) {
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		// TODO: process this
    /*    if(kN2PCalPruebanWidgetID==theSelectedWidget && theChange==kTrueStateMessage)
		{
           	DateX d;
			d.year = this->GetAnoSelected();;
			d.month = this->GetMesSelected();
			d.day = 1;
	       	this->printmonth(d);
		}
     */
		if (theSelectedWidget == kN2PCalComboBoxMesStatusWidgetID && theChange == kPopupChangeStateMessage)
		{
			DateX d;
			d.year = this->GetAnoSelected();;
			d.month = this->GetMesSelected();
			d.day = 1;
	        this->printmonth(d);
			this->CambiaFechaString();
		}
		if (theSelectedWidget == kN2PCalComboBoxAnoWidgetID && theChange == kPopupChangeStateMessage)
		{
			DateX d;
			d.year = this->GetAnoSelected();;
			d.month = this->GetMesSelected();
			d.day = 1;
	        this->printmonth(d);
			this->CambiaFechaString();
		}
	} while (kFalse);
}

int32 N2PCalDialogObserver::GetAnoSelected()
{
	PMString stringAno="";
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent==nil)
		{
			break;
		}
        
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
        {
            break;
        }
        
		IControlView* ComboCView = panelData->FindWidget(kN2PCalComboBoxAnoWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			ASSERT_FAIL("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		
        
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("No pudo Obtener dropListData*");
			break;
		}
		
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
        if(IDDLDrComboBoxSelecPrefer==nil)
        {
            ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
            break;
        }
        
		int32 index=IDDLDrComboBoxSelecPrefer->GetSelected();
		
		stringAno=dropListData->GetString(index);
		
	}while(false);
    return(stringAno.GetAsNumber());
}

int32 N2PCalDialogObserver::GetMesSelected()
{
	int32 Mesindex=-1;
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent==nil)
		{
			break;
		}
        
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
        {
            break;
        }
        
		IControlView* ComboCView = panelData->FindWidget(kN2PCalComboBoxMesStatusWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			ASSERT_FAIL("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		
        
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("No pudo Obtener dropListData*");
			break;
		}
		
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
        if(IDDLDrComboBoxSelecPrefer==nil)
        {
            ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
            break;
        }
        
		Mesindex=IDDLDrComboBoxSelecPrefer->GetSelected();
	}while(false);
    return(Mesindex+1);
}

void N2PCalDialogObserver::printmonth(DateX d)  // pointer to day value
{
    
	do
	{
		InterfacePtr<IWidgetParent>myParent(this, UseDefaultIID());
		if(myParent == nil)
		{
			break;
		}
		
		InterfacePtr<IPanelControlData>panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
        {
            break;
        }
        
		InterfacePtr<IN2PCalTableCellData>pTableCellData1((IN2PCalTableCellData*)myParent->QueryInterface(IID_IN2PCALTABLECELLDATA));
		if(pTableCellData1 == nil)
		{
			ASSERT_FAIL("Error1");
		}
		
		for(int32 Col=0;Col<pTableCellData1->GetMaxColumn();Col++)
			for(int32 Row=0;Row<pTableCellData1->GetMaxRow();Row++)
			{
				pTableCellData1->SetCellString(WideString(""), Col,Row);
                
				InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
				if(tableCell == nil)
                    break;
                
				tableCell->Invalidate();
			}
        
		d.day = 1;
		int index = DiaEnLaSemana(d);
        
		int counter = 0;
		for(  counter = 0;counter < index;counter++)
		{
			pTableCellData1->SetCellString(WideString("-"), counter,0);
			InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
			if(tableCell == nil)
				break;
			
			tableCell->Invalidate();
			
		}
        
		int32 row=0;
		for(int dia = 1;dia <= XX[GetYearType(d.year)][d.month];counter++,dia++)
		{
			if(counter%7 == 0 && dia == 1)
			{
                PMString NumDayString="";
                NumDayString.AppendNumber(dia);
                
                pTableCellData1->SetCellString(WideString(NumDayString), index,row);
                
                InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
                if(tableCell == nil) break;
                
                tableCell->Invalidate();
			}
			else
			{
				PMString NumDayString="";
				NumDayString.AppendNumber(dia);
                
				pTableCellData1->SetCellString(WideString(NumDayString), index,row);
                
				InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
				if(tableCell == nil) break;
                
				tableCell->Invalidate();
                
				if(counter%7 == 0)
				{
					index=0;
					row++;
					
                    
					PMString NumDayString="";
					NumDayString.AppendNumber(dia);
                    
					pTableCellData1->SetCellString(WideString(NumDayString), index,row);
                    
					InterfacePtr<IControlView> tableCell(panelData->FindWidget(kN2PCalTableCellWidgetID), UseDefaultIID());
					if(tableCell == nil)
						break;
					tableCell->Invalidate();
				}
			}
			index++;
		}
	}while(false);
}

//METODO QUE CAMBIA LA FECHA A UNA CADENA

void N2PCalDialogObserver::CambiaFechaString()
{
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent == nil)
		{
            CAlert::InformationAlert("myParent");
			break;
		}
		
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
        {
            CAlert::InformationAlert("panel");
            break;
        }
        
		InterfacePtr<IN2PCalTableCellData>	pTableCellData((IN2PCalTableCellData*)myParent->QueryInterface(IID_IN2PCALTABLECELLDATA));
		if(pTableCellData == nil)
		{
			CAlert::InformationAlert("Error1");
		}
		
		int32 row =pTableCellData->GetSelectRow();
		int32 column =pTableCellData->GetSelectColumn();
		
		if(row>=0 && column>=0)
		{
			InterfacePtr<IControlView>		editBoxView(panel->FindWidget(kN2PCalTableCellEditBoxWidgetID), UseDefaultIID());
			if(editBoxView == nil) break;
            
			InterfacePtr<ITextControlData>	selectedChar(editBoxView, UseDefaultIID());
			if(selectedChar == nil) break;
            
			PMString assetPathTitle(kN2PCalIdiomaStringkey);
			assetPathTitle.Translate();
			assetPathTitle.SetTranslatable(kTrue);
            
			PMString NumDiadeMes = pTableCellData->GetCellString(column, row);
			NumDiadeMes.SetTranslatable(kFalse);
            
			///Get Año
			IControlView* ComboCViewComboBoxAno = panel->FindWidget(kN2PCalComboBoxAnoWidgetID);
			ASSERT(ComboCViewComboBoxAno);
			if(ComboCViewComboBoxAno == nil)
			{
				CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
				break;
			}
            
			InterfacePtr<IStringListControlData> dropListDataComboBoxAno(ComboCViewComboBoxAno,IID_ISTRINGLISTCONTROLDATA);
			if (dropListDataComboBoxAno == nil)
			{
				CAlert::InformationAlert("No pudo Obtener dropListDataComboBoxAno*");
				break;
			}
            
			InterfacePtr<IDropDownListController>	IDDLDrComboBoxAnoPrefer( ComboCViewComboBoxAno, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxAnoPrefer==nil)
			{
				CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
				break;
			}
			
			PMString stringAno="";
			if(IDDLDrComboBoxAnoPrefer->GetSelected()>=0)
			{
				stringAno = dropListDataComboBoxAno->GetString(IDDLDrComboBoxAnoPrefer->GetSelected());
			}
			else
			{
				break;
			}
            
			//Get Mes
			IControlView* ComboCView = panel->FindWidget(kN2PCalComboBoxMesStatusWidgetID);
			ASSERT(ComboCView);
			if(ComboCView == nil)
			{
				CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
				break;
			}
            
			InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
			if (dropListData == nil)
			{
				CAlert::InformationAlert("No pudo Obtener dropListData*");
				break;
			}
            
			InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
				break;
			}
			
			PMString MesString ="";
			PMString nmnk="";
			nmnk.AppendNumber(IDDLDrComboBoxSelecPrefer->GetSelected());
            
			if(IDDLDrComboBoxSelecPrefer->GetSelected()>=0)
			{
				MesString = dropListData->GetString(IDDLDrComboBoxSelecPrefer->GetSelected());
				MesString.Translate();
				MesString.SetTranslatable(kTrue);
			}
			else
			{
				break;
			}
			
            
			PMString Dia="";
            
			switch(column)
			{
				case 1:
					Dia.Append(kN2PCalLunesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
                    break;
                    
				case 2:
					Dia.Append(kN2PCalMartesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
                    break;
                    
				case 3:
					Dia.Append(kN2PCalMiercolesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
                    break;
                    
				case 4:
					Dia.Append(kN2PCalJuevesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
                    break;
                    
				case 5:
					Dia.Append(kN2PCalViernesStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
                    break;
                    
				case 6:
					Dia.Append(kN2PCalSabadoStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
                    break;
                    
				case 0:
					Dia.Append(kN2PCalDomingoStringkey);
					Dia.Translate();
					Dia.SetTranslatable(kTrue);
					
                    break;
			}
            
			PMString DateX="";
			//CAlert::InformationAlert(DateX);
			if(NumDiadeMes.NumUTF16TextChars()>0 && NumDiadeMes.GetAsNumber()>0)
			{
				//Forma Fecha por Idioma
				if(assetPathTitle=="English")
				{
					DateX.Append(Dia);
					DateX.Append(", ");
					DateX.Append(MesString);
					DateX.Append(" ");
					DateX.Append(NumDiadeMes);
					DateX.Append(", ");
					DateX.Append(stringAno);
				}
				else
				{
					if(assetPathTitle=="Espanol")
					{
						DateX.Append(Dia);
						DateX.Append(" ");
						DateX.Append(NumDiadeMes);
						DateX.Append(" de ");
						DateX.Append(MesString);
						DateX.Append("  de ");
						DateX.Append(stringAno);
					}
				}
				//CAlert::InformationAlert("Z1= "+DateX);
				selectedChar->SetString(DateX, kTrue, kFalse);
			}
			else
			{
				//CAlert::InformationAlert("Z2= "+DateX);
				selectedChar->SetString(DateX, kTrue, kFalse);
			}
			//CAlert::InformationAlert("Z3= "+DateX);
			selectedChar->SetString(DateX, kTrue, kFalse);
		}
	}while(false);
}

// DIA EN LA SEMANA
unsigned int N2PCalDialogObserver::DiaEnLaSemana(DateX &d)
{
	unsigned int qty = 0;
	for(unsigned int y = origen.year;y < d.year;y++)
		qty += XX[GetYearType(y)][0];
	int index = GetYearType(d.year);
	for(unsigned int m = 1;m < d.month;m++)
		qty += XX[index][m];
	qty += d.day;
	return qty%7;
} 

// GET YEAR TYPE
unsigned int N2PCalDialogObserver::GetYearType(unsigned int year)
{
	if(year%100 == 0) // si year es divisible por 4 pero termina en 00
        year /= 100; // es BISIESTOX solamente cuando es divisible por 400
	if(year%4) // year no es divisible por 4,
        return COMUNX; // por eso no es BISIESTOX.
	return BISIESTOX;
} 

//  Code generated by DollyXs code generator
